<?php

require_once("animal.php");
require_once("frog.php");
require_once("ape.php");

    
$sheep = new Animal("shaun");

echo "Nama:  ". $sheep->name . "<br>"; // "shaun"
echo "legs: ". $sheep->legs . "<br>"; // 4
echo "cold blooded: ". $sheep->cold_blooded . "<br>"; // "no"
echo "<br> <br>";

$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"

echo "Nama:  ". $kodok->name . "<br>";
echo "legs: ". $kodok->legs . "<br>";
echo "cold blooded: ". $kodok->cold_blooded . "<br>"; 
echo "jump: " . $kodok->jump();
echo "<br> <br>";

$sungokong = new ape("kera sakti");
$sungokong->yell(); // "Auooo"

echo "Nama:  ". $sungokong->name . "<br>";
echo "legs: ". $sungokong->legs . "<br>";
echo "cold blooded: ". $sungokong->cold_blooded . "<br>"; 
echo "Yell: " . $sungokong->yell();
echo "<br> <br>";

?>